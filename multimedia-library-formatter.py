#!/usr/bin/env python3

import re
import os
import subprocess
import sys

def display_help():
    print('usage: [-hr] files...')

files = []
done = False
flags = {
    'no-rename' : False,
    }

# Grab all the args and flags, and when we reach a non-arg, assume that's the
# start of the files
for arg in sys.argv[1:]:
    if done == True:
        files.append(arg)
    else:
        matched = False
        if re.match(r'-[^\s]*h[^\s]*', arg) or re.match(r'--help', arg):
            matched = True
            display_help()
        if re.match(r'-[^\s]*r[^\s]*', arg) or re.match(r'--no-rename', arg):
            matched = True
            flags['no-rename'] = True
        if matched == False:
            files.append(arg)
            done = True

#TODO: Make renaming function for cleanliness
#TODO: Make what we convert images and video into as varriables
for file in files:
    if not os.path.exists(file):
        sys.stderr.write(file + ' does not exist!\n')
        continue
    if os.path.isdir(file):
        sys.stderr.write(file + ' is a directory, ignoring\n')
        continue
    if os.path.dirname(file):
        dir = os.path.dirname(file) + '/'
    else:
        dir = ''
    basename = os.path.basename(file)
    output = dir + 'output'
    # Match images
    if (re.match(r'.*.jpe?g$', file) or re.match(r'.*png$', file)
        or re.match(r'.*webp$', file)):
        output += '.webp'
        process = subprocess.run(['convert', file, '-quality', '100',
            '-define', 'webp:lossless=true', output])
        if process.returncode:
            process = subprocess.run(['rm', '-v', output])
            continue
        if flags['no-rename']:
            old_output = output
            output = dir + os.path.splitext(basename)[0] + '.webp'
            process = subprocess.run(['mv', '-v', old_output, output])
            if process.returncode:
                continue
            process = subprocess.run(['rm', '-v', file])
            if process.returncode:
                continue
        else:
            old_output = output
            process = subprocess.run(['sha512sum', output], capture_output=True)
            if process.returncode:
                continue
            output = dir + process.stdout.decode('utf-8').split()[0] + '.webp'
            process = subprocess.run(['mv', '-v', old_output, output])
            if process.returncode:
                continue
            process = subprocess.run(['rm', '-v', file])
            if process.returncode:
                continue
    # match video
    elif (re.match(r'.*.mp4$', file) or re.match(r'.*webm$', file)
        or re.match(r'.*gif$', file)):
        output += '.webm'
        process = subprocess.run(['ffprobe', file])
        crf = input('Please enter a CRF for this video: ')
        process = subprocess.run(['ffmpeg', '-i', file, '-y', '-c:v', 'libvpx-vp9',
            '-crf', crf, '-b:v', '0', '-deadline', 'best', '-pass', '1', '-an', '-f', 'webm', 
            '-row-mt', '1', '/dev/null'])
        process = subprocess.run(['ffmpeg', '-i', file, '-y',  '-c:v', 'libvpx-vp9',
            '-crf', crf, '-b:v', '0', '-deadline', 'best', '-pass', '2', '-c:a', 'libopus',
            '-row-mt', '1', output])
        subprocess.run(['rm', '-v', 'ffmpeg2pass-0.log'])
        if process.returncode:
            process = subprocess.run(['rm', '-v', output])
            continue
        if flags['no-rename']:
            old_output = output
            output = dir + os.path.splitext(basename)[0] + '.webm'
            process = subprocess.run(['mv', '-v', old_output, output])
            if process.returncode:
                continue
            process = subprocess.run(['rm', '-v', file])
            if process.returncode:
                continue
        else:
            old_output = output
            process = subprocess.run(['sha512sum', output], capture_output=True)
            if process.returncode:
                continue
            output = dir + process.stdout.decode('utf-8').split()[0] + '.webm'
            process = subprocess.run(['mv', '-v', old_output, output])
            if process.returncode:
                continue
            process = subprocess.run(['rm', '-v', file])
            if process.returncode:
                continue
    else:
        if not flags['no-rename']:
            # If we don't know what it is, and we are renaming, just rename
            # after its sha512 hash without conversion
            process = subprocess.run(['sha512sum', file], capture_output=True)
            if process.returncode:
                continue
            output = dir + process.stdout.decode('utf-8').split()[0] \
                + os.path.splitext(file)[1]
            process = subprocess.run(['mv', '-v', file, output])
            if process.returncode:
                continue
